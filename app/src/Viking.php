<?php

namespace Tournament;

use Exception;

class Viking extends Unit
{
	public $hitPoints = 120;

	/**
	 * @throws Exception
	 */
	public function __construct($rank = null)
	{
		parent::__construct($rank);
		$this->equip('axe');
	}
}