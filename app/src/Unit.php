<?php

namespace Tournament;

use Exception;
use Tournament\Equipments\Armor;
use Tournament\Equipments\Buckler;
use Tournament\Equipments\EquipmentFactory;
use Tournament\Equipments\PropertiesCollection;
use Tournament\Equipments\Weapon;

abstract class Unit
{
	public $hitPoints;

	private $hit = 0;

	/**
	 * @var Weapon $weapon
	 */
	protected $weapon;
	/**
	 * @var Armor $armor
	 */
	protected $armor;

	/**
	 * @var Buckler $buckler
	 */
	protected $buckler;

	public function setWeapon(Weapon $weapon)
	{
		$this->weapon = $weapon;
	}

	public function getWeapon(): Weapon
	{
		return $this->weapon;
	}

	public function setArmor(Armor $armor)
	{
		$this->armor = $armor;
	}

	public function setBuckler(?Buckler $buckler)
	{
		$this->buckler = $buckler;
	}

	public function __construct($rank = null)
	{
		if ($rank) {
			$this->setRank($rank);
		}
	}

	protected $rank;

	public function setRank(string $rank)
	{
		$this->rank = $rank;
	}

	public function engage(Unit $unit)
	{
		do {

			$this->attack($unit);

			if ($this->hitPoints <= 0 || $unit->hitPoints <= 0) {
				break;
			}

			$unit->attack($this);

		} while (!$unit->isDead() && !$this->isDead());
	}

	public function isDead()
	{
		return $this->hitPoints() == 0;
	}
	protected function attack(Unit $unit)
	{
		if ($this->weapon->readyToHit()) {
			$this->hit($unit);
		}
	}

	private function canBlock( ): bool
	{
		return $this->buckler && $this->buckler->canBlock();
	}


	public function getBuckler(): Buckler
	{
		return $this->buckler;
	}
	private function hit(Unit $unit)
	{
		if(!$unit->canBlock()) {
			$unit->takeDamage($unit->getTakeDamage($this->getIncurredDamage($unit)));
		} else {
			$unit->getBuckler()->apply($this);
		}

		$unit->hit++;
	}

	private function getIncurredDamage(Unit $target)
	{
		return $this->applyArmorPenalty($this->applyRanksPerks($this->weapon->getAttackPower(), $target), $target);
	}

	private function applyArmorPenalty($damage, Unit $target)
	{
		if ($this->armor) {
			return $this->armor->reduceDamageGiven($damage);
		}

		return $damage;
	}

	private function takeDamage(int $damage)
	{

		$this->hitPoints = max($this->hitPoints - $damage, 0);
	}

	public function hitPoints()
	{
		return $this->hitPoints;
	}

	private function getTakeDamage(int $damage): int
	{
		if ($this->armor) {
			return $this->armor->reduceDamageReceived($damage);
		}

		return $damage;
	}
	/**
	 * @throws Exception
	 */
	public function equip($equipment): Unit
	{
		$equipment = EquipmentFactory::factory($equipment, $this);

		$equipment->setEquipment($this);

		return $this;
	}
	protected function applyRanksPerks($damage, Unit $target)
	{
		return $damage;
	}

	/**
	 * @return int
	 */
	public function getHit(): int
	{
		return $this->hit;
	}
}