<?php

namespace Tournament;

class Highlander extends Unit
{
	public $hitPoints = 150;

	/**
	 * @throws \Exception
	 */
	public function __construct($rank = null)
	{
		parent::__construct($rank);
		$this->equip('great_sword');
	}

	protected function applyRanksPerks($damage, Unit $target)
	{
		if ($this->rank === 'Veteran' && $this->hitPoints() <= 0.3 * 150) {
			$damage *= 2;
		}

		return $damage;
	}
}