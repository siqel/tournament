<?php
namespace Tournament;

use Exception;

class Swordsman extends Unit
{
	public $hitPoints = 100;

	/**
	 * @throws Exception
	 */
	public function __construct($rank = null)
	{
		parent::__construct($rank);
		$this->equip('sword');

	}

	protected function applyRanksPerks($damage, Unit $target)
	{
		if ($this->rank === 'Vicious' && $target->getHit() < 2) {
			$damage += 20;
		}
		return $damage;
	}
}
