<?php

namespace Tournament\Equipments;

use Tournament\Unit;

abstract class Weapon extends Equipment
{
	private $attackPower;

	public function __construct(Unit $owner)
	{
		parent::__construct($owner);
		$this->apply();
	}

	public function setEquipment(Unit $unit)
	{
		$this->owner->setWeapon($this);
	}


	public function readyToHit(): bool
	{
		return true;
	}

	public function canBreakBuckler(): bool
	{
		return false;
	}

	public function getAttackPower()
	{
		return $this->attackPower;
	}

	public function setAttackPower(int $damage)
	{
		$this->attackPower = $damage;
	}
}