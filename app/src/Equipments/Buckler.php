<?php

namespace Tournament\Equipments;

use Tournament\Unit;

class Buckler extends Equipment
{
	private $hit = 0;

	protected $times_blocked = 0;

	public function apply(Unit $attacker)
	{
		if ($attacker->getWeapon()->canBreakBuckler()) {
			$this->times_blocked++;
			if ($this->times_blocked == 3)  {
				$this->owner->setBuckler(null);
			}
		}
	}
	public function setEquipment(Unit $unit)
	{
		$unit->setBuckler($this);
	}

	public function canBlock(): bool
	{
		return $this->hit++ % 2 == 0;
	}

}