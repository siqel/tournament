<?php

namespace Tournament\Equipments;

use Tournament\Unit;

abstract class Equipment
{

	protected $owner;

	public function __construct(Unit $owner)
	{
		$this->owner = $owner;
	}

	public $name;

	abstract public function setEquipment(Unit $unit);

}