<?php

namespace Tournament\Equipments;

class Sword extends Weapon
{
	public function apply()
	{
		$this->setAttackPower(5);
	}
}