<?php

namespace Tournament\Equipments;

use Tournament\Unit;

class Axe extends Weapon
{
	public function apply()
	{
		$this->setAttackPower(6);
	}

	public function canBreakBuckler(): bool
	{
		return true;
	}
}