<?php

namespace Tournament\Equipments;

class GreatSword extends Weapon
{
	private $hit = 0;

	public function apply()
	{
		$this->setAttackPower(12);
	}

	public function readyToHit(): bool
	{
		return ++$this->hit % 3 != 0;
	}
}