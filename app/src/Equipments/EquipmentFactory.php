<?php

namespace Tournament\Equipments;

use Tournament\Unit;

class EquipmentFactory
{
	protected const equipmentType = [
		'armor' => Armor::class,
		'buckler' => Buckler::class,
		'axe' => Axe::class,
		'great_sword' => GreatSword::class,
		'sword' => Sword::class
	];

	/**
	 * @throws \Exception
	 */
	public static function factory(string $equipment, Unit $unit): Equipment
	{
		if (!array_key_exists($equipment, self::equipmentType)) {
			throw new \Exception('invalid equipment ' . $equipment);
		}
		$classname = self::equipmentType[$equipment];

		return new $classname($unit);
	}
}