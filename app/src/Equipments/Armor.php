<?php

namespace Tournament\Equipments;

use Tournament\Unit;

class Armor extends Equipment
{
	public function setEquipment(Unit $unit)
	{
		$unit->setArmor($this);
	}

	public function reduceDamageReceived($damage): int
	{
		return $damage - 3;
	}

	public function reduceDamageGiven($damage): int
	{
		return $damage - 1;
	}
}